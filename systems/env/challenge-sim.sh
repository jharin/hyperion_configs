#!/usr/bin/env bash

######## TODO
#source ./paths.sh
export prefix="/vol/tiago/melodic-robocup/"
#export prefix_="/vol/tiago/robocup-challenge-vikings/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"
##########
export GAZEBO_MODEL_PATH="${prefix}/share/rcsim_gazebo/models:${GAZEBO_MODEL_PATH}"

#Robot Setup
export basepc=$(hostname -s)
export laptop=${basepc}
export robot=${basepc}
export ROS_MASTER_URI=http://${basepc}:11311
export ROBOT_VERSION="steel"
export SIMMODE="true"

#Simulation map/world
#export SIMULATION_WORLD="carry_my_luggage_simple"
export SIMULATION_WORLD="../../rcsim_gazebo/worlds/storing_groceries_static"
#export SIMULATION_WORLD="clf"
#export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_furnished2-sim.yaml"
#export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_map.yaml"
export NAVIGATION_MAP="/home/jharin/Documents/robocup/robocup_data/maps/storing_groceries.yaml"
#export NAVIGATION_MAP="/home/jharin/Documents/robocup/rc-sim/rcsim_gazebo/worlds/storing_groceries.world"
#export TIAGO_SPAWN_POSITION="x:=2.34 y:=-3 Y:=1.5 " #storing_groceries.
# Pose where the robot is spawned
export SPAWN_POSE_X="2"
export SPAWN_POSE_Y="-4"
export SPAWN_POSE_ANGLE="1.5"
#export NAVIGATION_MAP="/home/jharin/Documents/robocup/robocup_data/maps/carry_my_luggage_simple.yaml"
#export TIAGO_SPAWN_POSITION="x:=-1 y:=11 Y:=-1.5 "
# Pose where the robot is spawned
#export SPAWN_POSE_X="-1"
#export SPAWN_POSE_Y="11"
#export SPAWN_POSE_ANGLE="-1.5"

#export SIMULATION_WORLD="furnished_clf_actor"

#Pocketsphinx_grammars
export PSA_CONFIG="${PATH_TO_PSA_CONFIG}/tasks/restaurant/restaurant.conf"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

#object
#export PATH_TO_CLASSIFIERS="${prefix}/share/robocup_data/tensorflow"
#export PATH_TO_CLASSIFIERS="/home/jharin/Documents/projects/robocup_data/tensorflow"
#export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/md_openchallenge/frozen_inference_graph.pb"
#export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/md_openchallenge/label_map.pbtxt"
#export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/recognition/md_openchallenge"
#export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
#export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

export PATH_TO_CLASSIFIERS="/home/jharin/Documents/robocup/robocup_data/tensorflow"
export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/challenge/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/challenge/label_map.pbtxt"
export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/recognition/challenge/output_final_fin"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"


#export PATH_TO_CLASSIFIERS="${prefix}/share/storing_groceries_node/object"
#export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/frozen_inference_graph.pb"
#export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/label_map.pbtxt"
#export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/recognition"
#export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
#export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"
#export OBJECT_DETECTION_THRESHOLD="0.5"

export GRASPING_OBJECTS_CONFIG="${prefix}/share/storing_groceries_node/config/challenge2020.yaml" #neue wenn ich mit ycb Objekten arbeite


# KBASE locations
export PATH_TO_MONGOD_CONFIG="${prefix}/share/robocup_data/mongod/mongod.conf"
export PATH_TO_KBASE_CONFIG="${prefix}/share/robocup_data/knowledgeBase/configs/use_storing_groceries_db.yaml"
export PATH_TO_EDIT_KBASE_CONFIG="${prefix}/share/robocup_data/knowledgeBase/configs/edit_storing_groceries_db.yaml"


# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
