#!/usr/bin/env bash

######## TODO
#source ./paths.sh
export prefix="/vol/tiago/melodic-robocup/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix}/share/tiago_clf_nav/data"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix}/share/SpeechRec/psConfig"
##########

#Robot Setup
export basepc=$(hostname -s)
export laptop=${basepc}
export robot=${basepc}
export ROS_MASTER_URI=http://${basepc}:11311
export ROBOT_VERSION="steel"
export SIMMODE="true"

#Simulation map/world
export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_furnished2-sim.yaml"
export SIMULATION_WORLD="furnished_clf_actor"

#Pocketsphinx_grammars TODO Exercise 5
#export PSA_CONFIG="<path_to_your_psa_config>"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

#object
export PATH_TO_CLASSIFIERS="${prefix}/share/storing_groceries_node/object"
export OBJECT_DET_GRAPH="/home/jharin/Documents/projects/robocup_data/tensorflow/detection/challenge/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="/home/jharin/Documents/projects/robocup_data/tensorflow/detection/challenge/label_map.pbtxt"
export OBJECT_REC_PATH="/home/jharin/Documents/projects/robocup_data/tensorflow/recognition/challenge/output_final_fin"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
