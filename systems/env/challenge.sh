#!/usr/bin/env bash

######## TODO
#source ./paths.sh
#export prefix="/vol/tiago/melodic-robocup/"

export prefix="/vol/robocup/tiago-clf-moveitmaster/"
export prefix_="/vol/tiago/robocup-challenge-vikings/"
export PATH="${prefix}/bin:$PATH"

#ROS source alias
#export setup_suffix=$(echo $SHELL | cut -d "/" -f3-)
export setup_suffix="bash"
alias source_ros="source ${prefix}/setup.${setup_suffix}"

#Map/World paths
export PATH_TO_MAPS="${prefix_}/share/robocup_data/maps"

#PocketSphinx paths
export PATH_TO_PSA_CONFIG="${prefix_}/share/SpeechRec/psConfig"
export alsa_device="plug_tiago_mono"
##########

#Robot Setup
export basepc=$(hostname -s)
export laptop=${basepc}
export robot=tiago-47c
export ROS_MASTER_URI=http://${robot}:11311
export ROBOT_VERSION="steel"
export SIMMODE="true"


#Simulation map/world
export NAVIGATION_MAP="${PATH_TO_MAPS}/challenge/arena_vikings_challenge.yaml"
#export SIMULATION_WORLD="clf"
#export NAVIGATION_MAP="${PATH_TO_MAPS}/clf_furnished2-sim.yaml"
#export SIMULATION_WORLD="furnished_clf_actor"

#Pocketsphinx_grammars
export VDEMO_PSA_CONFIG="${PATH_TO_PSA_CONFIG}/exercise/challenge.conf"

#Rviz config
export RVIZ_CONFIG="${prefix}/share/tobi_sim/config/tiago.rviz"

# Path to bonsai config
export PATH_TO_BONSAI_ROBOCUP_CONFIG="${prefix}/opt/bonsai_robocup_addons/etc/bonsai_configs"
export PATH_TO_BONSAI_ROBOCUPTASKS_CONFIG="${prefix}/opt/bonsai_robocup_exercise/etc/bonsai_configs"
export PATH_TO_BONSAI_TIAGO_CONFIG="${prefix}/opt/bonsai_tiago_addons/etc/bonsai_configs"

# Path to scxml locations
export PATH_TO_BONSAI_ROBOCUP_SCXML="${prefix}/opt/bonsai_robocup_addons/etc/state_machines"
export PATH_TO_BONSAI_CORE_SCXML="${prefix}/opt/bonsai-scxml_engine/etc/behaviors"
export PATH_TO_BONSAI_ROBOCUPTASKS_SCXML="${prefix_}/opt/robocup-exercise/etc/state_machines"
export PATH_TO_BONSAI_PEPPER_SCXML="${prefix}/opt/bonsai2-pepper-dist/etc/state_machines"
export PATH_TO_BONSAI_TIAGO_SCXML="${prefix}/opt/bonsai_tiago_addons/etc/state_machines"

# Create mapping variable, used by bonsai2 to resolve "src=" attributes in scxml files
export BONSAI_MAPPING="ROBOCUP=${PATH_TO_BONSAI_ROBOCUP_SCXML} SCXML=${PATH_TO_BONSAI_CORE_SCXML} EXERCISE=${PATH_TO_BONSAI_ROBOCUPTASKS_SCXML} PEPPER=${PATH_TO_BONSAI_PEPPER_SCXML} TIAGO=${PATH_TO_BONSAI_TIAGO_SCXML}"

##########

#object
export PATH_TO_CLASSIFIERS="${prefix_}/share/robocup_data/tensorflow"
export OBJECT_DET_GRAPH="${PATH_TO_CLASSIFIERS}/detection/challenge/frozen_inference_graph.pb"
export OBJECT_DET_LABELS="${PATH_TO_CLASSIFIERS}/detection/challenge/label_map.pbtxt"
export OBJECT_REC_PATH="${PATH_TO_CLASSIFIERS}/recognition/challenge/output_final_fin"
export OBJECT_REC_GRAPH="${OBJECT_REC_PATH}/output_graph.pb"
export OBJECT_REC_LABELS="${OBJECT_REC_PATH}/output_labels.txt"

# KBASE locations
export PATH_TO_MONGOD_CONFIG="${prefix_}/share/robocup_data/mongod/mongod.conf"
export PATH_TO_KBASE_CONFIG="${prefix_}/share/robocup_data/knowledgeBase/configs/use_vikings_challenge.yaml"
export PATH_TO_EDIT_KBASE_CONFIG="${prefix_}/share/robocup_data/knowledgeBase/configs/edit_vikings_challenge.yaml"

# Other
export ACTOR_VEL_TOPIC="/Olf/cmd_vel"
